#ifndef SRC_GREP_S21_GREP_H_
#define SRC_GREP_S21_GREP_H_
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <regex.h>
#define LEN 1024

#define FLAG_i (1U << 2U)
#define FLAG_v (1U << 3U)
#define FLAG_c (1U << 4U)
#define FLAG_l (1U << 5U)
#define FLAG_n (1U << 6U)
#define FLAG_h (1U << 7U)
#define FLAG_s (1U << 8U)
#define FLAG_o (1U << 9U)
#define FLAG_f (1U << 10U)

void make_flags(int argc, char *argv[]);
void getprint(FILE* file, unsigned flags, char reg[LEN], char* fname);
FILE* file_open(char* fname, unsigned flags);
void reg_from_files(FILE* file, char reg[LEN]);
void my_free(char* str);
void make_color(char* str, char* sub_str, char* color);
int make_reg(char* str, char* reg, unsigned flags, char* grep_file, size_t num_str);
void print_str(char* str, char* fname, size_t str_counter, unsigned flags, int len, int* my_breake);
#endif  // SRC_GREP_S21_GREP_H_
